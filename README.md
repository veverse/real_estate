# VeVerse real estate smart contracts

ERC721 [upgradable](https://docs.openzeppelin.com/upgrades-plugins/1.x/) contract to represent property rights for apartments on VeVerse core space station.

# Installation

Create local `.secret` file with the following env variables: `ETHERSCAN_API_KEY`, `RINKEBY_URL`, `PRIVATE_KEY`, `REPORT_GAS`

Install [hardhat](https://hardhat.org/getting-started/#installation) and try running some of the following tasks:

```shell
npx hardhat accounts
npx hardhat compile
npx hardhat clean
npx hardhat test
npx hardhat node
npx hardhat help
REPORT_GAS=true npx hardhat test
npx hardhat coverage
npx hardhat run scripts/deploy.js
node scripts/deploy.js
npx eslint '**/*.js'
npx eslint '**/*.js' --fix
npx prettier '**/*.{json,sol,md}' --check
npx prettier '**/*.{json,sol,md}' --write
npx solhint 'contracts/**/*.sol'
npx solhint 'contracts/**/*.sol' --fix
```

## Etherscan verification

To try out Etherscan verification, you first need to deploy a contract to an Ethereum network that's supported by Etherscan, such as Ropsten.

In this project, copy the .env.example file to a file named .env, and then edit it to fill in the details. Enter your Etherscan API key, your Ropsten node URL (eg from Alchemy), and the private key of the account which will send the deployment transaction. With a valid .env file in place, first deploy your contract:

```shell
hardhat run --network ropsten scripts/deploy.js
```

Then, copy the deployment address and paste it in to replace `DEPLOYED_CONTRACT_ADDRESS` in this command:

```shell
npx hardhat verify --network ropsten DEPLOYED_CONTRACT_ADDRESS "Hello, Hardhat!"
```

# Deployments

## Rinkeby

* RealEstateNFT (Proxy): https://rinkeby.etherscan.io/address/0x8a5007EedB09B4cB423eE12773e1C8f43741D335
* RealEstateNFT (Implementation): https://rinkeby.etherscan.io/address/0x56bb3198df456909c3a617cd2a7eff560572eadb 
