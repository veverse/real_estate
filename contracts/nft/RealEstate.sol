// SPDX-License-Identifier: MPL-2.0

pragma solidity 0.8.10;

import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/PausableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC721/ERC721Upgradeable.sol";

/** 
 * @title Appartment NFT. 
 *
 * @dev represent ownership rights to apartments in VeVerse core world.
 *
 * Upgradable by VeVerse DAO.
 */
contract RealEstateNFT is ERC721Upgradeable, OwnableUpgradeable, PausableUpgradeable {
    using Strings for uint256;
    
    uint256 public constant MAX_APARTMENT_ID = 999;
    uint256 public maxMintableTokenId;
    
    // Prices for NFT minting in gas coin (Ether, Matic, BNB etc)
    mapping(uint256 => uint256) public prices;
    
    /** 
     * @dev Setup apartments NFT.
     */
    function initialize() external initializer {
        __Ownable_init();
        __Pausable_init();
        __ERC721_init("VeVerse Real Estate", "VRE");
        _pause();
        maxMintableTokenId = MAX_APARTMENT_ID;
    }

    /** 
     * @dev Mint real estate NFT for a gas coin fee.
     *
     * @param _tokenId 0-999 - apartment NFTs
     * @param _maxBatchId max id for NFT in a current minting batch, 999 for apartments
     */
    function mint(uint256 _tokenId, uint256 _maxBatchId) external payable whenNotPaused {
        require(_tokenId <= maxMintableTokenId && _tokenId <= _maxBatchId, "token not implemented");
        uint256 _price = prices[_maxBatchId];
        require(_price > 0 && _price == msg.value, "invalid price");

        // solhint-disable avoid-low-level-calls
        (bool _succeed,) = payable(owner()).call{value: msg.value}("");
        require(_succeed, "failed to pay");

        _mint(_msgSender(), _tokenId);
    }

    /** 
     * @dev Set price for NFT in _gasCoinAmount by owner.
     *
     * @param _maxBatchId type of NFT to mint
     * @param _gasCoinAmount price in gas coin for minting
     * @param _maxMintableTokenId regulates current minting phase, set 0 to stop
     */
    function setMintingRules(uint256 _maxBatchId, uint256 _gasCoinAmount, uint256 _maxMintableTokenId) external onlyOwner {
        require(_gasCoinAmount >= 0, "overflow of price");
        prices[_maxBatchId] = _gasCoinAmount;
        maxMintableTokenId = _maxMintableTokenId;
    }

    /**
     * @dev enables owner to pause / unpause minting.
     *
     * @param _paused true to pause, false to continue
     */
    function setPaused(bool _paused) external onlyOwner {
        if (_paused) _pause();
        else _unpause();
    }

    /** 
     * @dev Render token.
     *
     * @param _tokenId specific token to render.
     * @return generated base64 encoded JSON metadata.
     */
    function tokenURI(uint256 _tokenId) public pure override returns (string memory) {
        string memory metadata = string(abi.encodePacked(
            '{"name": "VeVerse Apartment #',
            _tokenId.toString(),
            '","description": "Key to apartment on VeVerse core world space station",',
            '"image": "ipfs://QmfUjEXbALS3LqTENhUqDr3AAFjh7v4F8Yyepnnaxars2y',
            '", "attributes":',
            compileAttributes(_tokenId),
            "}"
        ));

        return string(abi.encodePacked(
            "data:application/json;base64,",
            base64(bytes(metadata))
        ));
    }

    /** Utilities */

    /**
    * @dev Sets the attributes of the NFTs
    *
    * @param _tokenId the token to get the attributes for
    * @return string with generated attributes.
    */
    function compileAttributes(uint256 _tokenId) internal pure returns (string memory) {
        string memory _type = "Other";
        if (_tokenId <= MAX_APARTMENT_ID) _type = "Apartment";

        return string(abi.encodePacked(
            "[",
            attributeForTypeAndValue("Type", _type, false),
            "]"
        ));
    }

    /**
     * @dev Generates an attribute for the attributes array in the ERC721 metadata standard
     *
     * @param traitType the trait type to reference as the metadata key
     * @param value the token"s trait associated with the key
     * @return a JSON dictionary for the single attribute
     */
    function attributeForTypeAndValue(string memory traitType, string memory value, bool isNumber) internal pure returns (string memory) {
        return string(abi.encodePacked(
            '{"trait_type":"',
            traitType,
            '","value":',
            isNumber ? "" : '"',
            value,
            isNumber ? "" : '"',
            "}"
        ));
    }

    /** BASE 64 - Written by Brech Devos */
  
    string internal constant TABLE = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

    /**
     * @dev Encode abi encoded byte string into base64 encoding
     *
     * @param data abi encoded byte string
     * @return base64 encoded string
     */
    function base64(bytes memory data) internal pure returns (string memory) {
        if (data.length == 0) return "";
        
        // load the table into memory
        string memory table = TABLE;

        // multiply by 4/3 rounded up
        uint256 encodedLen = 4 * ((data.length + 2) / 3);

        // add some extra buffer at the end required for the writing
        string memory result = new string(encodedLen + 32);

        // solhit-disable no-inline-assembly
        assembly {
            // set the actual output length
            mstore(result, encodedLen)
            
            // prepare the lookup table
            let tablePtr := add(table, 1)
            
            // input ptr
            let dataPtr := data
            let endPtr := add(dataPtr, mload(data))
            
            // result ptr, jump over length
            let resultPtr := add(result, 32)
            
            // run over the input, 3 bytes at a time
            for {} lt(dataPtr, endPtr) {}
            {
                dataPtr := add(dataPtr, 3)
                
                // read 3 bytes
                let input := mload(dataPtr)
                
                // write 4 characters
                mstore(resultPtr, shl(248, mload(add(tablePtr, and(shr(18, input), 0x3F)))))
                resultPtr := add(resultPtr, 1)
                mstore(resultPtr, shl(248, mload(add(tablePtr, and(shr(12, input), 0x3F)))))
                resultPtr := add(resultPtr, 1)
                mstore(resultPtr, shl(248, mload(add(tablePtr, and(shr( 6, input), 0x3F)))))
                resultPtr := add(resultPtr, 1)
                mstore(resultPtr, shl(248, mload(add(tablePtr, and(        input,  0x3F)))))
                resultPtr := add(resultPtr, 1)
            }
            
            // padding with "="
            switch mod(mload(data), 3)
            case 1 { mstore(sub(resultPtr, 2), shl(240, 0x3d3d)) }
            case 2 { mstore(sub(resultPtr, 1), shl(248, 0x3d)) }
        }
        
        return result;
    }
}