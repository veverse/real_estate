// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const { ethers, upgrades } = require("hardhat");

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');

  // Upgrade contract
  const RealEstateNFTContract = await ethers.getContractFactory("RealEstateNFT");
  const realEstateNFT = await upgrades.upgradeProxy("0x8a5007EedB09B4cB423eE12773e1C8f43741D335", RealEstateNFTContract);
  console.log("RealEstateNFT upgraded to:", realEstateNFT.address);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
